import React from "react";
import { Buffer } from "buffer";
import { useSearchParams } from "react-router-dom";

function App() {
  const [searchParams, setSearchParams] = useSearchParams();
  console.log(searchParams.get("objParam"));
  const objParam = JSON.parse(
    Buffer.from(searchParams.get("objParam"), "base64").toString()
  );
  console.log("ObjParam2: ", objParam);

  return (
    <div>
      {objParam.tagAdicionales.labelTag1} S/. {objParam.montoPago} <p />
      {objParam.tagAdicionales.labelTag2} {objParam.horaIngreso} <p />
      {objParam.tagAdicionales.labelTag3} {objParam.permanencia} minutos <p />
      {objParam.tagAdicionales.labelTag4}{" "}
      {objParam.tipoPago === "B" ? "Boleta" : "Factura"} <p />
    </div>
  );
}

export default App;
