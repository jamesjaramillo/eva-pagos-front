import Home from "./components/home";
import { Route, Routes } from "react-router-dom";
import "./App.css";
import React from "react";

function App() {
  return (
    <React.Fragment>
      <main className="container">
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <h1 className="navbar-brand mb-0 h1"> HEADER PORTALES </h1>
        </nav>
      </main>
      <Routes>
        <Route path="moduloPago" element={<Home />} />
        <Route path="/" element={<Home />} />
      </Routes>
    </React.Fragment>
  );
}

export default App;
